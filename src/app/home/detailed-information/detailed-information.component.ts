import { Component, OnInit, Input } from '@angular/core';

import { InvLink } from '~/core/inv-link.model';

@Component({
    selector: 'inv-detailed-information',
    templateUrl: 'detailed-information.html',
    styleUrls: ['./detailed-information.scss']

})
export class DetailedInformationComponent implements OnInit {
    @Input() title: string;
    @Input() copy: string;
    @Input() links: Array<InvLink>;

    constructor() { }

    ngOnInit() { }
}
