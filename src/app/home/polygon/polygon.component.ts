import { AfterViewInit, Component, Input } from '@angular/core';

import * as seen from 'seen';

@Component({
    selector: 'inv-polygon',
    templateUrl: './polygon.component.html',
    styleUrls: ['./polygon.component.scss']
})
export class PolygonComponent implements AfterViewInit {

    @Input() uniqueId: string;

    constructor() { }

    ngAfterViewInit() {
        this.initPolygon();
    }

    private initPolygon(): void {
        let context, dragger, height, j, len, noiser, ref, scene, shape, surf, width;

        width = 2000;

        height = 2000;

        shape = seen.Shapes.sphere(2).scale(800);
        shape.fill(seen.Colors.hex('#2498be'));

        scene = new seen.Scene({
            fractionalPoints: true,
            cullBackfaces: false,
            model: seen.Models['default']().add(shape),
            viewport: seen.Viewports.center(width, height)
        });

        context = seen.Context(this.uniqueId, scene).render();

        ref = shape.surfaces;
        for (j = 0, len = ref.length; j < len; j++) {
            surf = ref[j];
            surf.originals = surf.points.map(function(p) {
                return p.copy();
            });
            surf.fillMaterial.color.a = 160;
        }

        noiser = new seen.Simplex3D(Math.random());

        context.animate().onBefore(function(t, dt) {
            let i, k, l, len1, len2, n, p, ref1, ref2;
            ref1 = shape.surfaces;
            for (k = 0, len1 = ref1.length; k < len1; k++) {
                surf = ref1[k];
                ref2 = surf.points;
                for (i = l = 0, len2 = ref2.length; l < len2; i = ++l) {
                    p = ref2[i];
                    n = noiser.noise(p.x, p.y, p.z + t * 1e-4);
                    surf.points[i] = surf.originals[i].copy().multiply(1 + n / 3);
                }
                surf.dirty = true;
            }
            return shape.rotx(dt * 1e-4).rotz(-dt * 1e-4);
        }).start();

        dragger = new seen.Drag(document.getElementById(this.uniqueId), {
            inertia: true
        });

        dragger.on('drag.rotate', function(e) {
            let ref1, xform;
            xform = (ref1 = seen.Quaternion).xyToTransform.apply(ref1, e.offsetRelative);
            shape.transform(xform);
            return context.render();
        });
    }
}
