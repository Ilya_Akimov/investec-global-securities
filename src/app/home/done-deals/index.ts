import { TombstoneComponent } from './tombstone.component';
import { DoneDealsComponent } from './done-deals.component';
import { Tombstone } from '../../core/tombstone.model';

export {
    TombstoneComponent,
    DoneDealsComponent,
    Tombstone
}
