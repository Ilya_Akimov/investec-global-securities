import { Component, Input } from '@angular/core';
import { Tombstone } from '../../core/tombstone.model';

@Component({
    selector: 'inv-done-deals',
    templateUrl: './done-deals.component.html',
    styleUrls: ['./done-deals.component.scss']
})
export class DoneDealsComponent {
    options: any = {
        autoplay: true,
        dots: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: '.slider-arrow-left',
        nextArrow: '.slider-arrow-right',
        mobileFirst: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    dots: true
                }
            },
            {
                breakpoint: 0,
                settings: {
                    slidesToShow: 1,
                    dots: true
                }
            }
        ]
    };
    @Input() tombstones: Array<Tombstone>;

    constructor() { }
}
