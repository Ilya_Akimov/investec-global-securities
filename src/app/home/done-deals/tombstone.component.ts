import { Component, Input } from '@angular/core';

@Component({
    selector: 'inv-tombstone',
    templateUrl: './tombstone.component.html',
    styleUrls: ['./tombstone.component.scss']
})
export class TombstoneComponent {
    @Input() tombstone;
    constructor() { }
}
