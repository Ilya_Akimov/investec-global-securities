import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { Dashtile } from './dashboard/dashtile.model';
import { InvTab } from './hide-reveal-tabs/inv-tab.model';
import { InvLink } from '~/core/inv-link.model';
import { HomeService } from '~/core/home.service';
import { Quote } from './quote-module/quote/quote.model';
import { Research } from './research.model';
import { SecurityService } from '~/core/security.service';
import { Tombstone } from '~/core/tombstone.model';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    private errorMessage: string;
    private myInterval = 3000;
    private noWrapSlides = false;
    private dealIndex = 0;
    public research: Research[];
    public latestResearch: InvTab[];
    public selectedTab = new InvTab('',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '');
    public header = 'Latest Research';
    public exploreHeader: string;
    public exploreText: string;
    public exploreLinks: Array<InvLink>;
    public quotes: Array<Quote>;
    public doneDealOptions = {
        loop: true,
        responsiveClass: true,
        responsive: {
            0: {
                navigation: true,
                items: 1,
                slideSpeed: 1000,
                paginationSpeed: 1000,
                smartSpeed: 1000,
                fluidSpeed: 1000,
                autoplay: true,
            },
            480: {
                navigation: true,
                items: 3,
                slideSpeed: 1000,
                paginationSpeed: 1000,
                smartSpeed: 1000,
                fluidSpeed: 1000,
                autoplay: true,
            },

            768: {
                navigation: true,
                items: 4,
                slideSpeed: 1000,
                paginationSpeed: 1000,
                smartSpeed: 1000,
                fluidSpeed: 1000,
                autoplay: true,
            }
        }
    };
    public tombstones: Tombstone[];
    public applications: Array<Dashtile>;

    constructor(private _router: Router,
        private _homeService: HomeService,
        private _route: ActivatedRoute,
        private _securityService: SecurityService
    ) { }

    ngOnInit() {
        this.applications = [];

        // To Be moved to server side. Manual for now
        this.exploreHeader = 'Explore';
        this.exploreText = `Clients now have the ability to access our
                          proprietary research portal. This is more than
                           simply a repository of research but is a
                           valuable research tool in its own right.`;
        let link = new InvLink('Research Portal', 'https://securities.investec.co.uk/researchportal/authredirect');
        this.exploreLinks = [];
        this.exploreLinks.push(link);

        let quote = new Quote(
            '"Through distinctive ideas, high quality market advice and best execution, our specialist team will work relentlessly to deliver you out of the ordinary returns"',
            'Andrew Fitchie, Global Head of Securities',
            './assets/images/andrew.jpg',
            './assets/images/andrew.jpg',
            './assets/images/andrew.jpg',
            './assets/images/andrew.jpg',
            './assets/images/andrew.jpg',
            '',
            'Meet the team',
            ''
        );

        let quote2 = new Quote(
            '"Another Quote to show the rotation"',
            'Jamie Rollinson.',
            './assets/images/quote-ib-server.jpg',
            './assets/images/quote-ib-server.jpg',
            './assets/images/quote-ib-server.jpg',
            './assets/images/quote-ib-server.jpg',
            './assets/images/quote-ib-server.jpg',
            '',
            'A Different Link',
            ''
        );

        let researchApplication = new Dashtile(
            './assets/images/Research.png',
            './assets/images/Research.png',
            './assets/images/Research.png',
            './assets/images/Research.png',
            './assets/images/Research.png',
            'http://devldnvmibgena2.uk.corp.investec.com/researchportal/?AuthRedirect'
        );

        this.loadTombstones();

        this.applications.push(researchApplication);
        this.quotes = [];
        this.quotes.push(quote);
        this.quotes.push(quote2);

    }

    isLoggedIn() {
        return this._securityService.IsAuthorized;
    }

    private loadTombstones() {
        this._homeService.loadTombstonesList()
            .add(() => {
                this.tombstones = this._homeService.tombstoneList;
            })
    }
}
