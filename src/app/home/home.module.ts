import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BenefitsComponent } from './benefits/benefits.component';
import { ContactBannerComponent } from './contact-banner/contact_banner.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashtileComponent } from './dashboard/dashtile.component';
import { DetailedInformationComponent } from './detailed-information/detailed-information.component';
import { DoneDealsComponent, TombstoneComponent } from './done-deals';
import { HeroMessagingComponent } from './hero-messaging/hero-messaging.component';
import { HomeComponent } from './home.component'
import { HomeRoutingModule } from './home-routing.module'
import { InvTabsComponent, TabMobileComponent, TabDesktopComponent } from './hide-reveal-tabs';
import { PolygonComponent } from './polygon/polygon.component';
import { QuoteComponent } from './quote-module';
import { ShareModule } from '~/shared/shared.module';
import { SlickSliderComponent } from './slick-slider/slick-slider.component';
import { TextGridComponent } from './text-grid/text_grid.component';

@NgModule({
    imports: [
        CommonModule,
        HomeRoutingModule,
        ShareModule
    ],
    declarations: [
        BenefitsComponent,
        ContactBannerComponent,
        DashboardComponent,
        DashtileComponent,
        DetailedInformationComponent,
        DoneDealsComponent,
        HeroMessagingComponent,
        HomeComponent,
        InvTabsComponent,
        PolygonComponent,
        QuoteComponent,
        TabDesktopComponent,
        TabMobileComponent,
        TextGridComponent,
        TombstoneComponent,
        SlickSliderComponent
    ]
})
export class HomeModule { }
