import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'inv-dashtile',
    templateUrl: './dashtile.html',
    styleUrls: ['./dashtile.scss']
})
export class DashtileComponent implements OnInit {
    @Input() dashtile;
    constructor() { }

    ngOnInit() { }
}
