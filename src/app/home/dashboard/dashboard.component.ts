import { Component, Injector, Input } from '@angular/core';
import { Dashtile } from './dashtile.model';

@Component({
   selector: 'inv-dashboard',
   templateUrl: './dashboard.html',
   styleUrls: ['./dashboard.scss']
})

export class DashboardComponent {
  @Input() dashtiles: Array<Dashtile>;
  constructor() { }
  public GoToLink(link: string) {
    if (link.length > 0) {
      window.location.href = link;
    }
  }
  public GoToReseach() {
    window.location.href
    = 'http://devldnvmibgena2.uk.corp.investec.com/researchportal/?AuthRedirect';
  }

}
