export class Dashtile {
    constructor(
        public mobileImagePath: string,
        public mobileLandscapeImagePath: string,
        public tabletImagePath: string,
        public tabletLandscapeImagePath: string,
        public desktopImagePath: string,
        public link: string
    ) { }
}
