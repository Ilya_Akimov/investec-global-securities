import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'inv-contact',
    templateUrl: './contact_banner.component.html',
    styleUrls: ['./contact_banner.component.scss']
})
export class ContactBannerComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}
