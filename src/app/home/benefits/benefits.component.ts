import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'inv-benefits',
    templateUrl: './benefits.html',
    styleUrls: ['./benefits.scss']
})
export class BenefitsComponent implements OnInit {
    public summary: string;
    public header1: string;
    public copy1: string;
    public header2: string;
    public copy2: string;
    public header3: string;
    public copy3: string;
    constructor() { }

    ngOnInit() {
        this.summary = `We are a leading securities business with operations serving a range of international markets, including the UK, US, South Africa, Ireland, India and Hong Kong. We believe in teamwork, integrity and are passionate about delivering the highest level of client service.`;
        this.header1 = `Award winning global research`;
        this.copy1 = `Access our network of analysts and their award winning, distinctive research. We actively research over 400 stocks globally across a range of developed, emerging and frontier markets.`;
        this.header2 = `High quality execution`;
        this.copy2 = `We trade over X00 stocks globally. We are trusted to provide best execution and source liquidity across a range of venues and in some of the most challenging markets and shares.`;
        this.header3 = `Leading market advice`;
        this.copy3 = `Over many years we have built strong and trusted relationships with institutional investors, both domestically and internationally. We are recognised as providing leading advice and insights in our specialist markets.`;
    }
}
