import { AfterViewInit, Component, OnInit } from '@angular/core';

import { ScrollMagicService } from '~/core/scroll-magic.service';

@Component({
    selector: 'inv-hero-messaging',
    templateUrl: './hero-messaging.html',
    styleUrls: ['./hero-messaging-gradient.scss',
        '../../../styles/base/_component.scss',
        '../../../styles/base/_type.scss'
    ]
})
export class HeroMessagingComponent implements AfterViewInit, OnInit {
    public zebraSrc = '/assets/images/Zebra.png';
    public siteName: string;
    public strapLine: string;

    constructor(
        private scrollMagicService: ScrollMagicService
    ) {}

    ngAfterViewInit() {
        this.magicScrollSet();
    }

    ngOnInit() {
        this.strapLine = 'Global Securities';
        this.siteName = 'Ideas, advice and execution';
    }

    magicScrollSet() {
        this.scrollMagicService.scrollController.enabled(true);
        this.scrollMagicService.zebraAnimation();
    }
}
