import { Component, OnInit } from '@angular/core';

import { Column } from './column.model';

@Component({
    selector: 'inv-grid',
    templateUrl: './text_grid.component.html',
    styleUrls: ['./text_grid.component.scss']
})
export class TextGridComponent implements OnInit {
    public columns: Column[] = [];

    constructor() { }

    ngOnInit() {
        this.getColumns();
    }

    private getColumns() {
        this.columns.push(
            {
                title: 'UK',
                context: 'Investec’s UK securities team provides advice and execution across the UK and European markets to institutions, retail brokers and wealth managers. We research 16 sectors, covering the UK market from small AIM-listed companies to Main-market mega caps as well as a number of sectors on a pan European basis. Our dedicated Funds team [blah blah blah] and we provide bespoke insights into m&a transactions through our special situations team. We continue to be recognised as a leading player in the small and midcap space.',
            }
        );
        this.columns.push(
            {
                title: 'US',
                context: 'Investec’s UK securities team provides advice and execution across the UK and European markets to institutions, retail brokers and wealth managers. We research 16 sectors, covering the UK market from small AIM-listed companies to Main-market mega caps as well as a number of sectors on a pan European basis. Our dedicated Funds team [blah blah blah] and we provide bespoke insights into m&a transactions through our special situations team. We continue to be recognised as a leading player in the small and midcap space.',
            }
        );
        this.columns.push(
            {
                title: 'India',
                context: 'Investec’s UK securities team provides advice and execution across the UK and European markets to institutions, retail brokers and wealth managers. We research 16 sectors, covering the UK market from small AIM-listed companies to Main-market mega caps as well as a number of sectors on a pan European basis. Our dedicated Funds team [blah blah blah] and we provide bespoke insights into m&a transactions through our special situations team. We continue to be recognised as a leading player in the small and midcap space.',
            }
        );
        this.columns.push(
            {
                title: 'South Africa',
                context: 'Investec’s UK securities team provides advice and execution across the UK and European markets to institutions, retail brokers and wealth managers. We research 16 sectors, covering the UK market from small AIM-listed companies to Main-market mega caps as well as a number of sectors on a pan European basis. Our dedicated Funds team [blah blah blah] and we provide bespoke insights into m&a transactions through our special situations team. We continue to be recognised as a leading player in the small and midcap space.',
            }
        );
        this.columns.push(
            {
                title: 'Ireland',
                context: 'Investec’s UK securities team provides advice and execution across the UK and European markets to institutions, retail brokers and wealth managers. We research 16 sectors, covering the UK market from small AIM-listed companies to Main-market mega caps as well as a number of sectors on a pan European basis. Our dedicated Funds team [blah blah blah] and we provide bespoke insights into m&a transactions through our special situations team. We continue to be recognised as a leading player in the small and midcap space.',
            }
        );
        this.columns.push(
            {
                title: 'Hong Kong',
                context: 'Investec’s UK securities team provides advice and execution across the UK and European markets to institutions, retail brokers and wealth managers. We research 16 sectors, covering the UK market from small AIM-listed companies to Main-market mega caps as well as a number of sectors on a pan European basis. Our dedicated Funds team [blah blah blah] and we provide bespoke insights into m&a transactions through our special situations team. We continue to be recognised as a leading player in the small and midcap space.',
            }
        );
    }
}
