import { Quote } from './quote/quote.model';
import { QuoteComponent } from './quote/quote.component';

export {
    QuoteComponent,
    Quote
}