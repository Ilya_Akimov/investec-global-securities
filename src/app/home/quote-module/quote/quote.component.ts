import { Component, Input } from '@angular/core';

import { Quote } from './quote.model';

@Component({
    selector: 'inv-quote',
    templateUrl: './quote.component.html',
    styleUrls: ['./quote.component.scss']
})
export class QuoteComponent {

    @Input() quote: Quote;

    constructor() { }
}
