export class Quote {
    constructor(
        public quote: string,
        public copy: string,
        public mobileImagePath: string,
        public mobileLandscapeImagePath: string,
        public tabletImagePath: string,
        public tabletLandscapeImagePath: string,
        public desktopImagePath: string,
        public imageAlt: string,
        public linkText: string,
        public link: string,
    ) { }
}
