import { Component, OnInit, Input } from '@angular/core';
import { InvTab } from './';
import { Observable, Subscription } from 'rxjs/Rx';

@Component({
    selector: 'inv-tabs',
    templateUrl: 'inv-tabs.html',
    styleUrls: ['./tabs.scss']
})
export class InvTabsComponent implements OnInit {
    @Input() header: string;
    @Input() tabs: Array<InvTab>;
    @Input() selectedTab: InvTab;
    timer: Observable<number>;
    timerSubscription: Subscription;
    selectedIndex = 0;
    constructor() { }

    ngOnInit() {
         this.setTimer();
    }

    setTimer() {
        this.timer =  Observable.timer(5000, 5000);
        this.timerSubscription = this.timer.subscribe(() => {
            if(this.tabs === undefined) {return; }
            if (this.selectedIndex === (this.tabs.length - 1)) {
                this.selectedIndex = 0;
            }
            else {
                this.selectedIndex++;
            }

            this.selectedTab = this.tabs[this.selectedIndex];

        });
    }

    over() {
        this.timerSubscription.unsubscribe();
    }

    out() {
         this.setTimer();
    }

}
