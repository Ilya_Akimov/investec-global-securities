import { Component, OnInit, Input } from '@angular/core';
import { InvTab } from './';

@Component({
    selector: 'inv-tab-mobile',
    templateUrl: 'inv-tab-mobile.html',
    styleUrls: ['./tabs.scss']
})
export class TabMobileComponent implements OnInit {
    @Input() tab: InvTab;

    public tabOpen = false;
    constructor() { }

    ngOnInit() { }
}
