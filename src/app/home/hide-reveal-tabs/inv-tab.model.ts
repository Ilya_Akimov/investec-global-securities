export class InvTab {
  constructor(
   public header: string,
   public copy: string,
   public linkText: string,
   public mobileImagePath: string,
   public mobileLandscapeImagePath: string,
   public tabletImagePath: string,
   public tabletLandscapeImagePath: string,
   public desktopImagePath: string,
   public imageAlt: string
  ) {}
}