import { TabMobileComponent } from './inv-tab-mobile.component';
import { TabDesktopComponent } from './inv-tab-desktop.component';
import { InvTabsComponent } from './inv-tabs.component';
import { InvTab } from './inv-tab.model';

export {
    TabMobileComponent,
    TabDesktopComponent,
    InvTabsComponent,
    InvTab
};
