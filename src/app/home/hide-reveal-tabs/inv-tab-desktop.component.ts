import { Component, OnInit, Input } from '@angular/core';
import { InvTab } from './';

@Component({
    selector: 'inv-tab-desktop',
    templateUrl: 'inv-tab-desktop.html',
    styles: [`img { width: 90%; }`]
})
export class TabDesktopComponent implements OnInit {
    @Input() tab: InvTab;
    constructor() { }

    ngOnInit() { }
}