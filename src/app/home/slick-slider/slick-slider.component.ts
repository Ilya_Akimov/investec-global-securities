import { Component, Input, ElementRef, AfterContentInit} from '@angular/core';
declare var $: any;

@Component({
    selector: 'inv-slick-slider',
    templateUrl: './slick-slider.component.html',
    styleUrls: ['./slick-slider.component.scss']
})
export class SlickSliderComponent implements AfterContentInit {
    $slickElement: any;
    defaultOptions: any = {};

    @Input() options: any;

    constructor(private slickSlider: ElementRef) { }

    ngAfterContentInit() {
        Object.keys(this.options).forEach(key => this.defaultOptions[key] = this.options[key]);
        this.$slickElement = $(this.slickSlider.nativeElement).slick(this.defaultOptions);
    }
}
