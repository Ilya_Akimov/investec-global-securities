import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SecurityService } from '~/core/security.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    get userName() {
        return '' ; // this._securityService.UserName;
        // return this.oidc.profile ? this.oidc.profile.name : '';
    }

    get userLevel() {
        return '' ; // return this._securityService.ClientLevel;
    }
    public market: string;
    public errorMessage: string;
    public name: string;
    constructor(
        private _securityService: SecurityService,
        // private _router: Router,
        private _route: ActivatedRoute,
    ) {}

    ngOnInit() {
        this._securityService.Locations ?
            this.market = this._securityService.Locations[0] :
            this.market = 'GL';
        this._route.queryParams.map(params => {
            this.market = params['market'] || 'GL';
        });


        if (!this.isLoggedIn() && window.location.hash) {
            this._securityService.AuthorizedCallback();
        }
    }


    isLoggedIn() {
        // return this._securityService.IsAuthorized;
    }

    marketChange = (market: string) => {
    }
}
