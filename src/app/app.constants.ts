import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
    public client_id = 'securitiesOnline';
    public SSOUrl: string = 'https://securities-sso.investec.co.uk/';
    public RedirectUrl: string =  'http://devldnvmibgena2.uk.corp.investec.com';
    public Scopes: string = 'openid profile securities';
}
