import { Component, OnInit, ViewChild, AfterViewInit, NgZone, HostListener } from '@angular/core';

import { ScrollMagicService } from '~/core/scroll-magic.service';
import { SecurityService } from '~/core/security.service';

@Component({
    selector: 'inv-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {
    @ViewChild('ootoImage') ootoImage: any;

    private sub: any;

    get userName() {
        return  this._securityService.UserName;
    }

    get userLevel() {
        return  this._securityService.ClientLevel;
    }

    constructor(
        private _securityService: SecurityService,
        private _scrollMagicService: ScrollMagicService,
        private ngZone: NgZone
    ) {}

    ngOnInit() {}

    ngAfterViewInit() {
        this.magicScrollSet();
    }

    @HostListener('window:resize', [])
    public onWindowResize() {
        this.ngZone.run(() => {
            this.magicScrollSet();
        });
    }

    magicScrollSet() {
        this._scrollMagicService.scrollController.enabled(true);
        this._scrollMagicService.headerAnimation();
        this._scrollMagicService.zebraAnimation();
    }

    signIn() {
        // this._securityService.Authorize();
    }

    signOut() {
        // this._securityService.Logoff();
    }

    isLoggedIn() {
        // return this._securityService.IsAuthorized;
    }
}
