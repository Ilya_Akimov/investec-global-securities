import { Directive, AfterViewInit, OnDestroy, OnInit, ViewContainerRef } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/throttleTime';

import * as $ from 'jquery';

@Directive({
    selector: '[appScrolled]'
})
export class ScrolledDirective implements AfterViewInit, OnDestroy, OnInit {

    public static readonly classScrolled = 'scrolled';
    private readonly debounceTime = 200;
    private readonly extraOffset = 50;

    private debouncer: Subscription;
    private $container: JQuery;
    private $window: JQuery;

    constructor(
        private viewContainer: ViewContainerRef
    ) {}

    ngAfterViewInit() {
        this.$container = $(this.viewContainer.element.nativeElement);
        this.$window = $(window);
    }

    ngOnDestroy() {
        this.debouncer.unsubscribe();
    }

    ngOnInit() {
       this.setDebouncer();
    }

    private scrollEvent(): void {
        const topOffset = this.$container.offset().top;
        const scrollPosition = this.$window.scrollTop();
        const windowHeight = this.$window.height();

        if (scrollPosition + windowHeight >= topOffset + this.extraOffset) {
            this.$container.addClass(ScrolledDirective.classScrolled);
        }
    }

    private setDebouncer(): void {
        this.debouncer = Observable
            .fromEvent(window, 'scroll')
            .throttleTime(this.debounceTime)
            .subscribe(() => this.scrollEvent());
    }
}
