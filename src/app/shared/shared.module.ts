import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FooterComponent } from './footer/footer.component'
import { HeaderComponent } from './header/header.component'
import { ScrolledDirective } from './scrolled.directive'

@NgModule({
    declarations: [
        FooterComponent,
        HeaderComponent,
        ScrolledDirective
    ],
    imports: [
        CommonModule,
    ],
    exports: [
        CommonModule,
        FooterComponent,
        HeaderComponent,
        ScrolledDirective
    ],
})
export class ShareModule { }
