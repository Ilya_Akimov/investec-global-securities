import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { HttpClient } from './http-client';

@Injectable()
export class AccountManagementService {
    constructor(private _http: HttpClient) { }

    linkAccount(token: string) {
        let apiUrl = `api/account/link`;
        return this._http.post(apiUrl, JSON.stringify(token) )
          .catch(this.handleError);
    }

    isLinked() {
        let apiUrl = `api/account/link`;
        return this._http.get(apiUrl)
          .catch(this.handleError);
    }

    private handleError(error: Response) {
        // in a real world app, we may send the error to some remote logging
        // infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}
