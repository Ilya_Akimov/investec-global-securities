import { Injectable } from '@angular/core';

import { TimelineMax, TweenMax } from 'gsap';
import 'animation.gsap';
import * as ScrollMagic from 'ScrollMagic';

@Injectable()
export class ScrollMagicService {
    public scrollController: any;

    private tl: any;
    private tl2: any;
    private tl3: any;
    private scene: any;
    private scene2: any;
    private scene3: any;

    constructor() {
        this.scrollController = new ScrollMagic.Controller();
    }

    headerAnimation() {
        this.tl = new TimelineMax({ align: 'start' });
        const headerTween = TweenMax.to('.header__holder', 5, { css: { transform: 'translateY(-40px)' } } as any);
        const heightTween = TweenMax.to('.header__inner', 5, { css: { height: 100 } } as any);
        const logoTween = TweenMax.to('.logo__link', 5, { css: { width: 150 } } as any);
        const headerInnerTween = TweenMax.to('.header__inner', 5, { css: { transform: 'translateY(23px)' }} as any);
        const headerControlsTween = TweenMax.to('.header-controls', 5, { css: { transform: 'translateY(-4px)' }} as any);
        this.tl.add([headerTween, heightTween, logoTween, headerInnerTween, headerControlsTween], 0);

        // Update Tween Based on Screen Size
        this.scene = new ScrollMagic.Scene();
        this.scene2 = new ScrollMagic.Scene();
        this.scene3 = new ScrollMagic.Scene();

        this.scene.setTween(this.tl);
        this.scene.offset(20);
        this.scene.duration(250);
        this.scene2.offset(40);
        this.scene3.offset(30);

        this.tl3 = new TimelineMax({ align: 'start', paused: true });
        this.tl3.to('.header__ootoLogo', 0.4, { opacity: 1 }, 0)
            .to('.header__ootoLogo', 0, { transform: 'translateY(0px)' }, 0)
            .from('.header-logo__divider', 0.5, { height: 47 }, 0);

        this.scene3.on('leave', () => {
            this.tl3.play(0);
        });

        this.tl2 = new TimelineMax({ align: 'start', paused: true });
        this.tl2
            .to('.header__ootoLogo', 0.4, { opacity: 0, transform: 'translateY(-20px)' }, 0)
            .to('.header-logo__divider', 0.5, { height: 0 }, 0);

        this.scene2.on('enter', () => {
            this.tl2.play(0);
        });

        this.scene.addTo(this.scrollController);
        this.scene2.addTo(this.scrollController);
        this.scene3.addTo(this.scrollController);
    }

    zebraAnimation() {
        const tlZebra = new TimelineMax({ align: 'start' });

        const containerTween = TweenMax.to('.hero-messaging-gradient', 5, { css: { height: 100 } } as any);
        const textTween = TweenMax.to('.hero-messaging-gradient__image-overlay', 5, { css: { top: 8 } } as any);
        const mainTween = TweenMax.to('.main', 5, { css: { paddingTop: 60 } } as any);
        const zebraTween = TweenMax.to('.hero-messaging-gradient__zebra', 5, { css: { width: 220 } } as any);
        tlZebra.add([containerTween, textTween, mainTween, zebraTween], 0);

        const sceneZebra = new ScrollMagic.Scene();
        sceneZebra.setTween(tlZebra);
        sceneZebra.offset(20);
        sceneZebra.duration(250);
        sceneZebra.addTo(this.scrollController);
    }
}
