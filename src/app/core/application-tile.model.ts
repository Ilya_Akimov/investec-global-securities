export class ApplicationTile {
    constructor(
        public imageSource: Date,
        public imageAltText: string,
        public title: string,
        public applicationText: string
    ) { }
}
