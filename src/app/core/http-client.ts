import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { SecurityService } from './security.service';

@Injectable()
export class HttpClient {
    constructor(private _http: Http,
                private _securityService: SecurityService ) { }

    createAuthorisationHeader(authHeaders: Headers) {
        authHeaders.append('Authorization',
            'Bearer ' + this._securityService.GetToken());
    }

    get(url) {
        const headers = new Headers();
        this.createAuthorisationHeader(headers);
        return this._http.get(url, {
            headers: headers
        });
    }

    post(url, body) {
        const headers = new Headers();
        this.createAuthorisationHeader(headers);
        headers.append('Content-Type', 'application/mock-api');
        headers.append('Accept', 'application/mock-api');

        return this._http.post(url, body, {
            headers: headers
        });
    }

}
