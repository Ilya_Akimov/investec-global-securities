import * as _ from 'lodash';

export class Tombstone {
    title: string;
    date: string;
    headline: string;
    copy: string;
    mobileImagePath: string;
    mobileLandscapeImagePath: string;
    tabletImagePath: string;
    tabletLandscapeImagePath: string;
    desktopImagePath: string;
    imageAlt: string;
    linkText: string;
    link: string;

    constructor(rawData: Tombstone) {
        _.assignIn(this, rawData);
    }
}
