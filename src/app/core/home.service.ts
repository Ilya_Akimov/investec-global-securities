import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import * as _ from 'lodash';

import { CommonHelper } from './common.helper';
import { Tombstone } from './tombstone.model';

@Injectable()
export class HomeService {

    public readonly defaultTombstoneName = 'default';
    public readonly defaultVersionName = '';

    public tombstoneList: Tombstone[];

    private tombstonesListSubscription: Subscription;

    constructor (private http: Http) { }

    public loadTombstonesList(): Subscription {
        if (!this.tombstonesListSubscription) {
            this.tombstonesListSubscription = this.requestTombstonesList()
                .subscribe((tombstoneList: Tombstone[]) => {
                    this.tombstoneList = tombstoneList;
                });
        }

        return this.tombstonesListSubscription;
    }

    private getTombstonePath(tombstoneName: string, versionName: string): string {
        if (_.isEmpty(tombstoneName)) {
            tombstoneName = this.defaultTombstoneName;
            versionName = this.defaultVersionName;
        }

        if (_.isEmpty(versionName)) {
            return `/assets/mock-api/Tombstones/${tombstoneName}.json`;
        }

        return `/assets/mock-api/Tombstones/${tombstoneName}/${versionName}.json`;
    }

    private requestTombstonesList(): Observable<Tombstone[]> {
        return this.http.get('/assets/mock-api/tombstone.json')
            .map(CommonHelper.extractJsonData)
            .map(models => CommonHelper.castModels(models, Tombstone))
            .catch(CommonHelper.handleError);
    }
}
