import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router';

import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Configuration } from '../app.constants';

@Injectable()
export class SecurityService {
    public IsAuthorized: boolean;
    public HasAdminRole: boolean;
    public UserName: string;
    public ClientLevel: string;
    public Locations: string[];
    public LoginType: string;

    private headers: Headers;
    private storage: any;

    constructor(private _http: Http,
                private _configuration: Configuration,
                private _router: Router) {

        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/mock-api');
        this.headers.append('Accept', 'application/mock-api');
        this.storage = localStorage;

        if (this.retrieve('IsAuthorized') !== '') {
            this.HasAdminRole = this.retrieve('HasAdminRole');
            this.IsAuthorized = this.retrieve('IsAuthorized');
            this.UserName =  this.retrieve('UserName');
            this.ClientLevel = this.retrieve('ClientLevel');
            this.Locations = this.retrieve('Locations');
            this.LoginType = this.retrieve('LoginType');
        }
    }

    public GetToken(): any {
        const token =  this.retrieve('authorizationData');
        return token;
    }

    public ResetAuthorizationData() {
        this.store('authorizationData', '');
        this.store('authorizationDataIdToken', '');

        this.IsAuthorized = false;
        this.HasAdminRole = false;
        this.UserName = '';
        this.ClientLevel = '';
        this.Locations = null;
        this.store('HasAdminRole', false);
        this.store('IsAuthorized', false);
        this.store('UserName', '');
        this.store('ClientLevel', '');
        this.store('Locations', null);
        this.store('LoginType', '');
    }

    public SetAuthorizationData(token: any, id_token: any) {
        if (this.retrieve('authorizationData') !== '') {
            this.store('authorizationData', '');
        }

        this.store('authorizationData', token);
        this.store('authorizationDataIdToken', id_token);
        this.IsAuthorized = true;
        this.store('IsAuthorized', true);

        const data: any = this.getDataFromToken(token);
        this.ClientLevel = data.clientLevel;
        this.store('ClientLevel', this.ClientLevel);
        this.Locations = data.permission;
        const loginType = data.idp;
        if (loginType === 'idsvr') {
            this.LoginType = 'Standard';
        } else {
            this.LoginType = loginType;
        }
        this.store('Locations', this.Locations);
        this.store('LoginType', this.LoginType);

        for (let i = 0; i < data.role.length; i++) {
            if (data.role[i] === 'admin') {
                this.HasAdminRole = true;
                this.store('HasAdminRole', true);
            }
        }
    }

    public SetUserInfo(token: any) {
        const userinfoUrl = this._configuration.SSOUrl + 'connect/userinfo';
        const headers = new Headers();
        headers.append('Authorization',
        'Bearer ' + token);
        this._http.get(userinfoUrl, {
            headers: headers}
        )
        .map(res => {
            this.UserName = res.json().name;
            this.store('UserName', this.UserName );
            })
            .catch(error => {
                 console.error(error);
                return Observable.throw(error.json().error || 'Server error');
            }).subscribe();
    }

    public Authorize() {
        this.ResetAuthorizationData();

        console.log('BEGIN Authorize, no auth data');

        const authorizationUrl = this._configuration.SSOUrl + 'connect/authorize';
        const client_id = this._configuration.client_id;
        const redirect_uri = this._configuration.RedirectUrl;
        const response_type = 'id_token token';
        const scope = this._configuration.Scopes;
        const nonce = 'N' + Math.random() + '' + Date.now();
        const state = Date.now() + '' + Math.random();

        this.store('authStateControl', state);
        this.store('authNonce', nonce);
        console.log('AuthorizedController created. adding myautostate: '
                + this.retrieve('authStateControl'));

        const url =
            authorizationUrl + '?' +
            'response_type=' + encodeURI(response_type) + '&' +
            'client_id=' + encodeURI(client_id) + '&' +
            'redirect_uri=' + encodeURI(redirect_uri) + '&' +
            'scope=' + encodeURI(scope) + '&' +
            'nonce=' + encodeURI(nonce) + '&' +
            'state=' + encodeURI(state);

        window.location.href = url;
    }

    public AuthorizedCallback() {
        console.log('BEGIN AuthorizedCallback, no auth data');
        this.ResetAuthorizationData();

        const hash = window.location.hash.substr(1);

        const result: any = hash.split('&').reduce((map, item) => {
            const parts = item.split('=');
            map[parts[0]] = parts[1];
            return map;
        }, {});

        console.log(result);
        console.log('AuthorizedCallback created, begin token validation');

        let token = '';
        let id_token = '';
        let authResponseIsValid = false;
        if (!result.error) {

            if (result.state !== this.retrieve('authStateControl')) {
                console.log('AuthorizedCallback incorrect state');
            } else {

                token = result.access_token;
                id_token = result.id_token;

                const dataIdToken: any = this.getDataFromToken(id_token);
                console.log(dataIdToken);

                // validate nonce
                if (dataIdToken.nonce !== this.retrieve('authNonce')) {
                    console.log('AuthorizedCallback incorrect nonce');
                } else {
                    this.store('authNonce', '');
                    this.store('authStateControl', '');

                    authResponseIsValid = true;
                    console.log(`AuthorizedCallback state and nonce validated, returning access token`);
                }
            }
        }

        if (authResponseIsValid) {
            this.SetAuthorizationData(token, id_token);
            console.log(this.retrieve('authorizationData'));
            this.SetUserInfo(token);
            // this._router.navigateByUrl('');
        } else {
            this.ResetAuthorizationData();
            this._router.navigate(['']);
        }
    }

    public Logoff() {
        // /connect/endsession?id_token_hint=...
        // &post_logout_redirect_uri=https://myapp.com
        console.log('BEGIN Authorize, no auth data');

        const authorizationUrl = this._configuration.SSOUrl
            + 'connect/endsession';

        const id_token_hint = this.retrieve('authorizationDataIdToken');
        const post_logout_redirect_uri = '';

        const url =
            authorizationUrl + '?' +
            'id_token_hint=' + encodeURI(id_token_hint) + '&' +
            'post_logout_redirect_uri=' + encodeURI(post_logout_redirect_uri);

        this.ResetAuthorizationData();

        window.location.href = url;
    }

    public HandleError(error: any) {
        console.log(error);
        if (error.status === 403) {
            this._router.navigate(['/Forbidden']);
        } else if (error.status === 401) {
            this.ResetAuthorizationData();
            this._router.navigate(['']);
        }
    }

    private urlBase64Decode(str) {
        let output = str.replace('-', '+').replace('_', '/');
        switch (output.length % 4) {
            case 0:
                break;
            case 2:
                output += '==';
                break;
            case 3:
                output += '=';
                break;
            default:
                throw new Error('Illegal base64url string!');
        }

        return window.atob(output);
    }

    private getDataFromToken(token) {
        let data = {};
        if (typeof token !== 'undefined') {
            const encoded = token.split('.')[1];
            data = JSON.parse(this.urlBase64Decode(encoded));
        }

        return data;
    }

    private retrieve(key: string): any {
        const item = this.storage.getItem(key);

        if (item && item !== 'undefined') {
            return JSON.parse(this.storage.getItem(key));
        }

        return;
    }

    private store(key: string, value: any) {
        this.storage.setItem(key, JSON.stringify(value));
    }

}
