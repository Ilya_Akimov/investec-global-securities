export class DoneDeals {
    constructor(
        public company: string,
        public imageLink: string,
        public details: string,
        public investecRole: string,
        public dateDone: string
    ) { }
}
