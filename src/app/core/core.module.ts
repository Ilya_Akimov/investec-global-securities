import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeService } from './home.service';
import { HttpClient } from './http-client';
import { ScrollMagicService } from './scroll-magic.service';
import { SecurityService } from './security.service';

@NgModule({
    imports: [
        CommonModule
    ],
    providers: [
        HomeService,
        HttpClient,
        ScrollMagicService,
        SecurityService
    ],
})
export class CoreModule {
    constructor (@Optional() @SkipSelf() parentModule: CoreModule) {
        if (parentModule) {
            throw new Error(
                'CoreModule is already loaded. Import it in the AppModule only');
        }
    }
}
