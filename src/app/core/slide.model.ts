export class Slide {
    constructor(
        public imageSource: string,
        public imageClass: string,
        public title: string,
        public description: string,
        public redirectUrl: string
    ) { }
}
