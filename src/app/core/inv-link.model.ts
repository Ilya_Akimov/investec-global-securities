export class InvLink {
    constructor(
        public linkText: string,
        public link: string
    ) { }
}
