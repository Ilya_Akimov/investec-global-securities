import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { HttpClient } from './http-client';

@Injectable()
export class FrontPageService {
    constructor(private _http: HttpClient) { }

    getDisplay<T>(displayType: string) {
        let apiUrl = `api/applicationdisplay/${displayType}`;
        return this._http.get(apiUrl)
          .map(res => <T>res.json())
          .catch(this.handleError);
    }

    private handleError(error: Response) {
        // we may send the error to some remote logging
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}
