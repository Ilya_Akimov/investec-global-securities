import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Configuration } from './app.constants';
import { CoreModule } from './core/core.module';
import { ShareModule } from './shared/shared.module';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        CoreModule,
        HttpModule,
        ShareModule
    ],
    providers: [
        Configuration
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
